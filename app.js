
const axios = require('axios');


function responseMessage(results) {
  let statusCode = 200;
  let data = results;
  let success = true;

  return {
    statusCode,
    headers: {
      "Access-Control-Allow-Headers": "*",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      "Access-Control-Allow-Credentials": true,
      Accept: "*/*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ success, data }),
  };
}

module.exports.handler = async (event, context) => {
  var requestBody = JSON.parse(event.body);

  var numero_documento_chatbot = requestBody.nro_documento;
  var tipo_documento_chatbot = requestBody.tipo_documento;
  var portal_chatbot = requestBody.categoria;
  var id_subcategoria_chatbot = requestBody.subcategoria;

  var message = "";
  var code = "";
  var landing = "";


  if (portal_chatbot == "1") {
    var category = "elcomercio";
  } else if (portal_chatbot == "2") {
    var category = "gestion";
  } else {
    var category = "";
  }




  const response = await new Promise((rsv, rjt) => {
    try {
      if (id_subcategoria_chatbot == "1") {
        let hdrs = {};
        hdrs.Authorization =
          "Bearer 109998623fcffa2086f406f8880b24942532d80fc5da0420047ab1ee22f6bdfa";

        let data = {
          document_number: numero_documento_chatbot,
          document_type: tipo_documento_chatbot,
          portal: category,
        };

        axios
        .get(
          "https://jjnkyl1a6f.execute-api.us-west-2.amazonaws.com/test/api/v1/subscriptions/print",
          {
            params: data,
            headers: hdrs,
          }
        )
          .then(function (response) {
            var len = (response['data']['subscriptions']).length;
            console.log(len);


            if (len > 0) {

              for (let i = 0; i < len; i++) {
                message += `${response['data']['subscriptions'][i].id} > ${response['data']['subscriptions'][i].product} - ${response['data']['subscriptions'][i].periodicity}   </br>`;
                console.log(message);
              };
              landing = response['data']['subscriptions'];
              code = "1";
              console.log(code);
              console.log(landing);

            } else {
              landing = "No tiene suscripciones";
              message = "No se encontro ningun informacion de suscripciones";
              code = "0";
              console.log(code);
              console.log(landing);
            }


            rsv({
              landing,
              message,
              code,
            });


          })
          .catch(function (error) {
            rjt({
              message: error.message,
              code: '0'
            });
          })

      } else if (id_subcategoria_chatbot == "2") {
        let hdrs = {};
        hdrs.Authorization =
          "Bearer 109998623fcffa2086f406f8880b24942532d80fc5da0420047ab1ee22f6bdfa";

        let data = {
          document_number: numero_documento_chatbot,
          document_type: tipo_documento_chatbot,
          portal: category,
        };

        axios
        .get(
          "https://jjnkyl1a6f.execute-api.us-west-2.amazonaws.com/test/api/v1/subscriptions/bundle",
          {
            params: data,
            headers: hdrs,
          }
        )
          .then(function (response) {
            var len = (response['data']['subscriptions']).length;
            console.log(len);


            if (len > 0) {

              for (let i = 0; i < len; i++) {
                message += `${response['data']['subscriptions'][i].id} > ${response['data']['subscriptions'][i].product} - ${response['data']['subscriptions'][i].periodicity}   </br>`;
                console.log(message);
              };
              landing = response['data']['subscriptions'];
              code = "1";


            } else {
              landing = "No tiene suscripciones";
              message = "No se encontro ningun informacion de suscripciones";
              code = "0";

            }


            rsv({
              landing,
              message,
              code,
            });


          })
          .catch(function (error) {
            rjt({
              message: error.message,
              code: '0'
            });
          })
      } else if (id_subcategoria_chatbot == "3") {
        let hdrs = {};
        hdrs.Authorization =
          "Bearer 109998623fcffa2086f406f8880b24942532d80fc5da0420047ab1ee22f6bdfa";

        let data = {
          document_number: numero_documento_chatbot,
          document_type: tipo_documento_chatbot,
          portal: category,
        };

        axios
        .get(
          "https://jjnkyl1a6f.execute-api.us-west-2.amazonaws.com/test/api/v1/subscriptions/digital",
          {
            params: data,
            headers: hdrs,
          }
        )
          .then(function (response) {
            var len = (response['data']['subscriptions']).length;
            console.log(len);

            
            if (len > 0) {

              for (let i = 0; i < len; i++) {
                message += `${response['data']['subscriptions'][i].id} > ${response['data']['subscriptions'][i].product} - ${response['data']['subscriptions'][i].periodicity}   </br>`;
                console.log(message);
              };
              landing = response['data']['subscriptions'];
              code = "1";
              console.log(code);
              console.log(landing);

            } else {
              landing = "No tiene suscripciones";
              message = "No se encontro ningun informacion de suscripciones";
              code = "0";
              console.log(code);
              console.log(landing);
            }
            

            rsv({
              landing,
              message,
              code,
            });


          })
          .catch(function (error) {
            rjt({
              message: error.message,
              code: '0'
            });
          })
      } else {
        rjt({
          message: 'No existe la category',
          code: '0'
        });
      }


    } catch (error) {
      rjt({
        message: error.message,
        code: '0'
      });
    }
  });

  return responseMessage(response);
};
